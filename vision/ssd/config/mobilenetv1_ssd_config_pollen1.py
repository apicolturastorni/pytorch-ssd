import numpy as np

from vision.utils.box_utils import SSDSpec, SSDBoxSizes, generate_ssd_priors_simple


image_size = 300
image_mean = np.array([127, 127, 127])  # RGB layout
image_std = 128.0
iou_threshold = 0.45
center_variance = 0.1
size_variance = 0.2


specs = [
    SSDSpec(19, 16, SSDBoxSizes(37, 34), [2, 3]),
    SSDSpec(19, 16, SSDBoxSizes(72, 56), [2, 3]),
    SSDSpec(19, 16, SSDBoxSizes(77, 116), [2, 3]),
    SSDSpec(19, 16, SSDBoxSizes(99, 76), [2, 3]),
    SSDSpec(19, 16, SSDBoxSizes(70, 85), [2, 3]),
    SSDSpec(19, 16, SSDBoxSizes(52, 61), [2, 3]),
    SSDSpec(19, 16, SSDBoxSizes(45, 74), [2, 3]),
    SSDSpec(19, 16, SSDBoxSizes(120, 120), [2, 3]),
    SSDSpec(19, 16, SSDBoxSizes(40, 49), [2, 3]),
    SSDSpec(19, 16, SSDBoxSizes(55, 41), [2, 3])
]
priors = generate_ssd_priors_simple(specs, image_size)

print(' ')
print('SSD-Mobilenet-v1 priors:')
print(priors.shape)
print(priors)
print(' ')

#import torch
#torch.save(priors, 'mb1-ssd-priors.pt')

#np.savetxt('mb1-ssd-priors.txt', priors.numpy())
