import numpy as np

from vision.utils.box_utils import SSDSpec, SSDBoxSizes, generate_ssd_priors_simple


image_size = 300
image_mean = np.array([127, 127, 127])  # RGB layout
image_std = 128.0
iou_threshold = 0.45
center_variance = 0.1
size_variance = 0.2

specs = [
    SSDSpec(19, 16, SSDBoxSizes(54, 28), [2, 3]),
    SSDSpec(19, 16, SSDBoxSizes(50, 64), [2, 3]),
    SSDSpec(19, 16, SSDBoxSizes(62, 42), [2, 3]),
    SSDSpec(19, 16, SSDBoxSizes(53, 54), [2, 3]),
    SSDSpec(19, 16, SSDBoxSizes(32, 55), [2, 3]),
    SSDSpec(19, 16, SSDBoxSizes(57, 47), [2, 3]),
    SSDSpec(19, 16, SSDBoxSizes(44, 61), [2, 3]),
    SSDSpec(19, 16, SSDBoxSizes(64, 50), [2, 3]),
    SSDSpec(19, 16, SSDBoxSizes(47, 47), [2, 3]),
    SSDSpec(19, 16, SSDBoxSizes(58, 57), [2, 3])
]


priors = generate_ssd_priors_simple(specs, image_size)

print(' ')
print('SSD-Mobilenet-v1 priors:')
print(priors.shape)
print(priors)
print(' ')

#import torch
#torch.save(priors, 'mb1-ssd-priors.pt')

#np.savetxt('mb1-ssd-priors.txt', priors.numpy())
