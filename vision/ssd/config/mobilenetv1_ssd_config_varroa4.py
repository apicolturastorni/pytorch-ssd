import numpy as np

from vision.utils.box_utils import SSDSpec, SSDBoxSizes, generate_ssd_priors_simple


image_size = 300
image_mean = np.array([127, 127, 127])  # RGB layout
image_std = 128.0
iou_threshold = 0.45
center_variance = 0.1
size_variance = 0.2

specs = [
    SSDSpec(19, 16, SSDBoxSizes(64, 47), [2, 3]),
    SSDSpec(19, 16, SSDBoxSizes(49, 62), [2, 3]),
    SSDSpec(19, 16, SSDBoxSizes(53, 44), [2, 3]),
    SSDSpec(19, 16, SSDBoxSizes(57, 55), [2, 3]),
    SSDSpec(19, 16, SSDBoxSizes(35, 55), [2, 3])
]


priors = generate_ssd_priors_simple(specs, image_size)

print(' ')
print('SSD-Mobilenet-v1 priors:')
print(priors.shape)
print(priors)
print(' ')

#import torch
#torch.save(priors, 'mb1-ssd-priors.pt')

#np.savetxt('mb1-ssd-priors.txt', priors.numpy())
